﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class FreezeRotation : MonoBehaviour {
	public float fixedX = 0.0f;
	public float fixedY = 0.0f;
	public float fixedZ = 0.0f;

	[Serializable]
	public struct FreezeRotations {
		public bool x;
		public bool y;
		public bool z;
	};

	public FreezeRotations freezeRotations;

	// Update is called once per frame
	void Update () {
		float newX = freezeRotations.x ? fixedX : transform.rotation.x;
		float newY = freezeRotations.y ? fixedY : transform.rotation.y;
		float newZ = freezeRotations.z ? fixedZ : transform.rotation.z;
		transform.rotation = new Quaternion(newX, newY, newZ, transform.rotation.w);
	}
}
