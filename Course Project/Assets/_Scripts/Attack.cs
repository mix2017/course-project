﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attack {
    public const int ATTACK_HEAL = 1;
    public const int ATTACK_NORMAL = 2;

    public const int EFFECT_NONE = 0;
    public const int EFFECT_HEAL = 1;
    public const int EFFECT_BUFF_ATTACK = 6;
    public const int EFFECT_BUFF_DEFENSE = 7;

    public string name;
    public int atkPower;
    public int effect;
    public int id;
    public Sprite icon;

    public Attack(string name, int atkPower, int effect)
    {
        this.name = name;
        this.atkPower = atkPower;
        this.effect = effect;

        switch(effect)
        {
            case EFFECT_HEAL:
                icon = GameController.Instance.GetAttackSprite(GameController.SPRITE_HEAL);
                break;

            case EFFECT_BUFF_ATTACK:
                icon = GameController.Instance.GetAttackSprite(GameController.SPRITE_BUFF_ATTACK);
                break;

            case EFFECT_BUFF_DEFENSE:
                icon = GameController.Instance.GetAttackSprite(GameController.SPRITE_BUFF_DEFENSE);
                break;

            default:
                icon = GameController.Instance.GetAttackSprite(GameController.SPRITE_ATTACK);
                break;
        }
    }
}
