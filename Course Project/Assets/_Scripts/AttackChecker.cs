﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackChecker : MonoBehaviour {

    public GameObject[] monsters;
    private string identificator;

    private float proximityRange = 7.0f;
	private Vector3 direction;

    void Start()
    {
		identificator = tag;
		direction = identificator.Equals(GameController.PLAYER_ONE_MONSTERS) ? Vector3.forward : Vector3.back;
		Debug.Log("AttackChecker initialized for " + identificator);
	}

    void SequentialAttackCheck()
    {
        foreach (GameObject monster in monsters)
        {
			GameObject origin = monster.GetComponent<Monster>().FixedPoint;
			RaycastHit[] hits = Physics.RaycastAll(origin.transform.position, direction, proximityRange);
            foreach (RaycastHit hit in hits)
            {
                if (GameController.GameState == GameController.STATE_NONE &&
                    hit.collider.tag.Equals(GameController.DefenderTag))
                {
                    GameObject target = hit.collider.gameObject;
                    GameController.Attacker = monster.GetComponent<Monster>();
                    GameController.Target = target.GetComponentInChildren<Monster>();
                    GameController.GameState = GameController.STATE_WAIT;
                    GameController.Attacker.ToggleAttackUI(true);
                    break;
                }
            }

            if (GameController.Attacker != null) break;
        }
    }

    bool HasReturnedFromCombat()
    {
        Vector3 origin = GameController.Target.FixedPoint.transform.position;
		RaycastHit[] hits = Physics.SphereCastAll(origin, proximityRange, direction);
        foreach (RaycastHit hit in hits)
        {
            if (hit.collider.tag.Equals(GameController.AttackerTag))
            {
                return false;
            }
        }

        return true;
    }

    void FixedUpdate()
    {
        if(identificator.Equals(GameController.AttackerTag))
        {
            if (GameController.Attacker == null) SequentialAttackCheck();
            else if(GameController.GameState == GameController.STATE_NEXT_TURN)
            {
                if (HasReturnedFromCombat())
                {
                    Debug.Log("Next turn...");
                    GameController.Attacker = null;
                    GameController.Target = null;
                    GameController.GameState = GameController.STATE_NONE;
                    GameController.Instance.SwitchTurn();
                }
            }
            else if(GameController.Attacker != null &&
                GameController.Target != null && 
                GameController.GameState == GameController.STATE_WAIT)
            {
                if(HasReturnedFromCombat())
                {
                    Debug.Log("Aborted attack");
                    GameController.Attacker.ToggleAttackUI(false);
                    GameController.Attacker = null;
                    GameController.Target = null;
                    GameController.GameState = GameController.STATE_NONE;
                }
            }
        }
    }

}
