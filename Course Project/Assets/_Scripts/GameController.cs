﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    [Serializable]
    public struct AttackSprite
    {
        public string name;
        public Sprite sprite;
    }

    // Sprite Constants
    public const string SPRITE_ATTACK = "Attack";
    public const string SPRITE_BUFF_ATTACK = "BuffAttack";
    public const string SPRITE_BUFF_DEFENSE = "BuffDefense";
    public const string SPRITE_HEAL = "Heal";

    // Constants
    public const string PLAYER_ONE_MONSTERS = "Player-1-Monster";
    public const string PLAYER_TWO_MONSTERS = "Player-2-Monster";

    public const int TURN_PLAYER_ONE = 1;
    public const int TURN_PLAYER_TWO = 2;

    public const int STATE_NONE = 0;
    public const int STATE_WAIT = 1;
    public const int STATE_NEXT_TURN = 2;

    // Game state and data
    public static Monster Attacker = null;
    public static Monster Target = null;
    public static int currentTurn = TURN_PLAYER_ONE;
    public static int GameState = STATE_NONE;

    // Helper properties
    public static string AttackerTag
    {
        get { return currentTurn == TURN_PLAYER_ONE ? PLAYER_ONE_MONSTERS : PLAYER_TWO_MONSTERS; }
    }

    public static string DefenderTag
    {
        get { return currentTurn == TURN_PLAYER_ONE ? PLAYER_TWO_MONSTERS : PLAYER_ONE_MONSTERS;  }
    }

    public GameObject MonsterUI;
    public GameObject AttackUI;
	public GameObject AttackColumns;
	public GameObject ProximityChecker;
    public AttackSprite[] attackSprites;

    private static GameController _instance;
    public static GameController Instance
    {
        get { return _instance; }
    }

    void Awake()
    {
        _instance = this;
    }

    public void SwitchTurn()
    {
        if (currentTurn == TURN_PLAYER_ONE)
            currentTurn = TURN_PLAYER_TWO;
        else
            currentTurn = TURN_PLAYER_ONE;
    }

    public Sprite GetAttackSprite(string name)
    {
        foreach (AttackSprite aSprite in attackSprites)
            if (aSprite.name.Equals(name)) return aSprite.sprite;

        return null;
    }
}
