﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class Monster : MonoBehaviour {
    public string monsterName;
    public int health;
    public int maxHealth;
    public int atkPower;
    public int defPower;
    public Guid Identifier;

    public Attack offensiveAttack;
    public Attack defensiveAttack;

    private Text namePlate;
    private UnityEngine.UI.Image healthBar;
    private Text battleText;

    private GameObject attacksUI;
	private Text hitLabel;

    private float offensiveAttackThreshold = -0.3f;
    private float defensiveAttackThreshold = 0.3f;

	private Vector3 direction;

	public GameObject FixedPoint;

    void Start()
    {
		hitLabel = GameObject.Find("HitTarget").GetComponent<Text>();
        InitializeMonsterUI();
        offensiveAttack = new Attack("Slash", 80, Attack.EFFECT_NONE);
        defensiveAttack = new Attack("Heal", 80, Attack.EFFECT_HEAL);
        Identifier = Guid.NewGuid();
        InitializeAttackUI();
		InitializeAttackColumns();
		direction = gameObject.name.Equals(GameController.PLAYER_ONE_MONSTERS) ? Vector3.forward : Vector3.back;
	}

    void Update()
    {
        if(GameController.GameState == GameController.STATE_WAIT &&
            GameController.Attacker != null &&
            GameController.Attacker.Identifier.Equals(Identifier))
        {
			RaycastHit[] hits = Physics.RaycastAll(transform.parent.position + new Vector3(0, 5.0f, 0), transform.TransformDirection(direction), 15.0f);
			foreach (RaycastHit hit in hits) {
				if (hit.collider.gameObject.name.Equals("Defensive")) {
					HandleAttackEnd();
					UseAttack(GameController.Target, defensiveAttack);
				}
				else if(hit.collider.gameObject.name.Equals("Offensive")) {
					HandleAttackEnd();
					UseAttack(GameController.Target, offensiveAttack);
				}
			}
        }
    }

    void HandleAttackEnd()
    {
        GameController.GameState = GameController.STATE_NEXT_TURN;
    }

    void InitializeAttackUI()
    {
        attacksUI = Instantiate(GameController.Instance.AttackUI, transform);
        attacksUI.transform.localScale = new Vector3(1, 1, 1);

		if (gameObject.name.Equals(GameController.PLAYER_TWO_MONSTERS)) {
			attacksUI.transform.localPosition = new Vector3(0, 0, 1.14f);
		} else {
			attacksUI.transform.localPosition = new Vector3(0, 0, 0);
		}

        foreach(Transform child in attacksUI.transform)
        {
            if (child.gameObject.name.Equals("OffensiveAttack"))
                child.GetComponent<SpriteRenderer>().sprite = offensiveAttack.icon;
            else
                child.GetComponent<SpriteRenderer>().sprite = defensiveAttack.icon;
        }

        attacksUI.SetActive(false);
    }

    public void ToggleAttackUI(bool toggle)
    {
        attacksUI.SetActive(toggle);
    }

	void InitializeAttackColumns() {
		GameObject attackColumns = Instantiate(GameController.Instance.AttackColumns, transform);
		attackColumns.transform.localScale = new Vector3(1, 1, 1);
		attackColumns.transform.localPosition = new Vector3(0, 0, 0);

		FixedPoint = Instantiate(GameController.Instance.ProximityChecker, transform);
		FixedPoint.transform.localScale = new Vector3(1, 1, 1);

		if (gameObject.name.Equals(GameController.PLAYER_ONE_MONSTERS))
			FixedPoint.transform.localPosition = new Vector3(0, 0.5f, 0.5f);
		else
			FixedPoint.transform.localPosition = new Vector3(0, 0.5f, -0.5f);
	}

    void InitializeMonsterUI()
    {
        GameObject monsterUI = Instantiate(GameController.Instance.MonsterUI, transform);
        RectTransform rectTransform = monsterUI.GetComponent<RectTransform>();
        rectTransform.anchoredPosition = new Vector3(0, 0.761f, 0);
        rectTransform.localPosition = new Vector3(0, 0.761f, 0);
        rectTransform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
        Text[] tComps = monsterUI.GetComponentsInChildren<Text>();
        foreach(Text t in tComps)
        {
            if(t.gameObject.name.Equals("BattleText"))
            {
                battleText = t;
            } else
            {
                namePlate = t;
            }
        }

        Transform[] children = monsterUI.GetComponentsInChildren<Transform>();

        foreach (Transform child in children)
        {
            if ("HealthBar".Equals(child.gameObject.name)) healthBar = child.gameObject.GetComponent<UnityEngine.UI.Image>();
            if (healthBar != null) break;
        }

        monsterUI.GetComponent<Canvas>().enabled = false;
    }

    public void UpdateUI()
    {
        healthBar.fillAmount = health / ((float) maxHealth);
        if (namePlate.text.EndsWith(" "))
        {
            namePlate.text = monsterName;
        } else
        {
            namePlate.text = monsterName + " ";
        }
    }

    public void Heal(int amount)
    {
        health += amount;
        if (health > maxHealth) health = maxHealth;
        UpdateUI();
        RunBattleText("+" + amount, Color.green);
    }

    public void Damage(int amount)
    {
        health -= amount;
        if (health < 0) health = 0;
        UpdateUI();
        RunBattleText("-" + amount, Color.red);
    }

    public int UseAttack(Monster target, Attack attack)
    {
        ToggleAttackUI(false);

        if (attack.effect == Attack.EFFECT_HEAL)
        {
            Heal(40);
            return Attack.ATTACK_HEAL;
        }

        float damage = (atkPower * attack.atkPower / 100) - target.defPower;
        if (damage > 0)
            target.Damage((int)damage);

        Debug.Log("Hit " + target.monsterName + " with " + damage + " damage");

        return Attack.ATTACK_NORMAL;
    }

    public void RunBattleText(string text, Color color)
    {
        if(battleText.text.EndsWith(" "))
        {
            battleText.text = text;
        } else
        {
            battleText.text = text + " ";
        }

        battleText.color = color;

        Animator anim = battleText.GetComponent<Animator>();
        anim.SetBool("RunAnim", true);
        StartCoroutine(WaitForAnim(anim));
    }

    IEnumerator WaitForAnim(Animator anim)
    {
        while (anim.GetCurrentAnimatorStateInfo(0).IsName("BattleText") || anim.GetCurrentAnimatorStateInfo(0).IsName("Idle"))
            yield return new WaitForSeconds(0.5f);

        anim.SetBool("RunAnim", false);
    }
}
